package com.btsinfo.applicationintro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private Spinner spinner1;
    private Button buttonLancer;
    private Spinner spinner2;
    TextView tvMonRecap;
    private String choixniveau;

    private String choixface;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        buttonLancer = (Button) findViewById(R.id.buttonLancer);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        tvMonRecap = (TextView) findViewById(R.id.tvMonRecap);

        Intent mIntent = getIntent();

        String result = mIntent.getStringExtra("Spinner2");


        final ArrayAdapter<CharSequence> adapter =ArrayAdapter.createFromResource(this,R.array.nombre__arrays,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixniveau = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(adapterView.getContext(),choixniveau, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        final ArrayAdapter<CharSequence> adapter2 =ArrayAdapter.createFromResource(this,R.array.faces,android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        spinner2.setAdapter(adapter2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixface = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(adapterView.getContext(),choixface, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buttonLancer.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),MainResult.class);
                startActivity(intent);
                toRecap();
            }
        });

    }
    public void toRecap(){
        Intent intent = new Intent(this,MainResult.class);
        intent.putExtra("Spinner",choixface);
        intent.putExtra("Spinner2",choixniveau);
        startActivity(intent);
    }
}



