package com.btsinfo.applicationintro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.security.SecureRandom;

public class MainResult extends AppCompatActivity {

    private Button buttonRetour;
    private Button buttonReLancer;

    ImageView tvResult;
    ImageView tvResult2;
    ImageView tvResult3;
    ImageView tvResult4;
    ImageView tvResult5;

    int monresult=0;
    int monresult2=0;
    int monresult3=0;
    int monresult4=0;
    int monresult5=0;

    ImageView result;
    TextView tvMonRecap;
    TextView tvMonRecap2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_result);
        tvMonRecap = (TextView) findViewById(R.id.tvMonRecap);
        tvMonRecap2 = (TextView) findViewById(R.id.tvMonRecap2);
        buttonRetour = (Button) findViewById(R.id.buttonRetour);
        buttonReLancer = (Button) findViewById(R.id.buttonReLancer);

        Intent mIntent = getIntent();
        String monrecap = "Vous avez choisit " + mIntent.getStringExtra("Spinner");
        final String choixface = mIntent.getStringExtra("Spinner");
        String choixniveaux = mIntent.getStringExtra("Spinner2");

        tvResult = (ImageView) findViewById(R.id.tvResult);
        tvResult2 = (ImageView) findViewById(R.id.tvResult2);
        tvResult3 = (ImageView) findViewById(R.id.tvResult3);
        tvResult4 = (ImageView) findViewById(R.id.tvResult4);
        tvResult5 = (ImageView) findViewById(R.id.tvResult5);

        SecureRandom ran = new SecureRandom();

        buttonRetour.setOnClickListener(new View.OnClickListener() {
        @Override

        public void onClick(View v) {

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            toRecap();
        }
    });

        if (choixniveaux.equals("1 dés")){
            tvResult2.setVisibility(View.VISIBLE);
            if (choixface.equals("4 faces")){
                monresult=ran.nextInt(4)+1;
                if (monresult == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
            }

            if (choixface.equals("6 faces")){
                monresult=ran.nextInt(6)+1;
                if (monresult == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
            }

            if (choixface.equals("8 faces")){
                monresult=ran.nextInt(8)+1;
                if (monresult == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
            }

            if (choixface.equals("10 faces")){
                monresult=ran.nextInt(10)+1;
                if (monresult == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
            }

            if (choixface.equals("12 faces")){
                monresult=ran.nextInt(12)+1;
                if (monresult == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
            }

            if (choixface.equals("20 faces")){
                monresult=ran.nextInt(20)+1;
                if (monresult == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
                if (monresult == 13){
                    tvResult2.setImageResource(R.drawable.de13);
                }
                if (monresult == 14){
                    tvResult2.setImageResource(R.drawable.de14);
                }
                if (monresult == 15){
                    tvResult2.setImageResource(R.drawable.de15);
                }
                if (monresult == 16){
                    tvResult2.setImageResource(R.drawable.de16);
                }
                if (monresult == 17){
                    tvResult2.setImageResource(R.drawable.de17);
                }
                if (monresult == 18){
                    tvResult2.setImageResource(R.drawable.de18);
                }
                if (monresult == 19){
                    tvResult2.setImageResource(R.drawable.de19);
                }
                if (monresult == 20){
                    tvResult2.setImageResource(R.drawable.de20);
                }
            }
        }

        if (choixniveaux.equals("2 dés")){
            tvResult.setVisibility(View.VISIBLE);
            tvResult3.setVisibility(View.VISIBLE);
            if (choixface.equals("4 faces")){
                monresult=ran.nextInt(4)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                monresult2=ran.nextInt(4)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
            }

            if (choixface.equals("6 faces")){
                monresult=ran.nextInt(6)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                monresult2=ran.nextInt(6)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
            }

            if (choixface.equals("8 faces")){
                monresult=ran.nextInt(8)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                monresult2=ran.nextInt(8)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
            }

            if (choixface.equals("10 faces")){
                monresult=ran.nextInt(10)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                monresult2=ran.nextInt(10)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
            }

            if (choixface.equals("12 faces")){
                monresult=ran.nextInt(12)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                monresult2=ran.nextInt(12)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
            }

            if (choixface.equals("20 faces")){
                monresult=ran.nextInt(20)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                if (monresult == 13){
                    tvResult.setImageResource(R.drawable.de13);
                }
                if (monresult == 14){
                    tvResult.setImageResource(R.drawable.de14);
                }
                if (monresult == 15){
                    tvResult.setImageResource(R.drawable.de15);
                }
                if (monresult == 16){
                    tvResult.setImageResource(R.drawable.de16);
                }
                if (monresult == 17){
                    tvResult.setImageResource(R.drawable.de17);
                }
                if (monresult == 18){
                    tvResult.setImageResource(R.drawable.de18);
                }
                if (monresult == 19){
                    tvResult.setImageResource(R.drawable.de19);
                }
                if (monresult == 20){
                    tvResult.setImageResource(R.drawable.de20);
                }
                monresult2=ran.nextInt(20)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
                if (monresult2 == 13){
                    tvResult3.setImageResource(R.drawable.de13);
                }
                if (monresult2 == 14){
                    tvResult3.setImageResource(R.drawable.de14);
                }
                if (monresult2 == 15){
                    tvResult3.setImageResource(R.drawable.de15);
                }
                if (monresult2 == 16){
                    tvResult3.setImageResource(R.drawable.de16);
                }
                if (monresult2 == 17){
                    tvResult3.setImageResource(R.drawable.de17);
                }
                if (monresult2 == 18){
                    tvResult3.setImageResource(R.drawable.de18);
                }
                if (monresult2 == 19){
                    tvResult3.setImageResource(R.drawable.de19);
                }
                if (monresult2 == 20){
                    tvResult3.setImageResource(R.drawable.de20);
                }
            }
        }

        if (choixniveaux.equals("3 dés")){
            tvResult.setVisibility(View.VISIBLE);
            tvResult2.setVisibility(View.VISIBLE);
            tvResult3.setVisibility(View.VISIBLE);
            if (choixface.equals("4 faces")){
                monresult=ran.nextInt(4)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                monresult2=ran.nextInt(4)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                monresult3=ran.nextInt(4)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
            }

            if (choixface.equals("6 faces")){
                monresult=ran.nextInt(6)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                monresult2=ran.nextInt(6)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                monresult3=ran.nextInt(6)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de4);
                }
            }

            if (choixface.equals("8 faces")){
                monresult=ran.nextInt(8)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                monresult2=ran.nextInt(8)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                monresult3=ran.nextInt(8)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
            }

            if (choixface.equals("10 faces")){
                monresult=ran.nextInt(10)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                monresult2=ran.nextInt(10)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                monresult3=ran.nextInt(10)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
            }

            if (choixface.equals("12 faces")){
                monresult=ran.nextInt(12)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                monresult2=ran.nextInt(12)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
                monresult3=ran.nextInt(12)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult3 == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult3 == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
            }

            if (choixface.equals("20 faces")){
                monresult=ran.nextInt(20)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                if (monresult == 13){
                    tvResult.setImageResource(R.drawable.de13);
                }
                if (monresult == 14){
                    tvResult.setImageResource(R.drawable.de14);
                }
                if (monresult == 15){
                    tvResult.setImageResource(R.drawable.de15);
                }
                if (monresult == 16){
                    tvResult.setImageResource(R.drawable.de16);
                }
                if (monresult == 17){
                    tvResult.setImageResource(R.drawable.de17);
                }
                if (monresult == 18){
                    tvResult.setImageResource(R.drawable.de18);
                }
                if (monresult == 19){
                    tvResult.setImageResource(R.drawable.de19);
                }
                if (monresult == 20){
                    tvResult.setImageResource(R.drawable.de20);
                }
                monresult2=ran.nextInt(20)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
                if (monresult2 == 13){
                    tvResult3.setImageResource(R.drawable.de13);
                }
                if (monresult2 == 14){
                    tvResult3.setImageResource(R.drawable.de14);
                }
                if (monresult2 == 15){
                    tvResult3.setImageResource(R.drawable.de15);
                }
                if (monresult2 == 16){
                    tvResult3.setImageResource(R.drawable.de16);
                }
                if (monresult2 == 17){
                    tvResult3.setImageResource(R.drawable.de17);
                }
                if (monresult2 == 18){
                    tvResult3.setImageResource(R.drawable.de18);
                }
                if (monresult2 == 19){
                    tvResult3.setImageResource(R.drawable.de19);
                }
                if (monresult2 == 20){
                    tvResult3.setImageResource(R.drawable.de20);
                }
                monresult3=ran.nextInt(20)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult3 == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult3 == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
                if (monresult3 == 13){
                    tvResult2.setImageResource(R.drawable.de13);
                }
                if (monresult3 == 14){
                    tvResult2.setImageResource(R.drawable.de14);
                }
                if (monresult3 == 15){
                    tvResult2.setImageResource(R.drawable.de15);
                }
                if (monresult3 == 16){
                    tvResult2.setImageResource(R.drawable.de16);
                }
                if (monresult3 == 17){
                    tvResult2.setImageResource(R.drawable.de17);
                }
                if (monresult3 == 18){
                    tvResult2.setImageResource(R.drawable.de18);
                }
                if (monresult3 == 19){
                    tvResult2.setImageResource(R.drawable.de19);
                }
                if (monresult3 == 20){
                    tvResult2.setImageResource(R.drawable.de20);
                }
            }
        }

        if (choixniveaux.equals("4 dés")){
            tvResult.setVisibility(View.VISIBLE);
            tvResult2.setVisibility(View.VISIBLE);
            tvResult3.setVisibility(View.VISIBLE);
            tvResult4.setVisibility(View.VISIBLE);
            if (choixface.equals("4 faces")){
                monresult=ran.nextInt(4)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                monresult2=ran.nextInt(4)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                monresult3=ran.nextInt(4)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                monresult4=ran.nextInt(4)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
            }

            if (choixface.equals("6 faces")){
                monresult=ran.nextInt(6)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                monresult2=ran.nextInt(6)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                monresult3=ran.nextInt(6)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                monresult4=ran.nextInt(6)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
            }

            if (choixface.equals("8 faces")){
                monresult=ran.nextInt(8)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                monresult2=ran.nextInt(8)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                monresult3=ran.nextInt(8)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                monresult4=ran.nextInt(8)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
            }

            if (choixface.equals("10 faces")){
                monresult=ran.nextInt(10)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                monresult2=ran.nextInt(10)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                monresult3=ran.nextInt(10)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                monresult4=ran.nextInt(10)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
                if (monresult4 == 9){
                    tvResult4.setImageResource(R.drawable.de9);
                }
                if (monresult4 == 10){
                    tvResult4.setImageResource(R.drawable.de10);
                }
            }

            if (choixface.equals("12 faces")){
                monresult=ran.nextInt(12)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                monresult2=ran.nextInt(12)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
                monresult3=ran.nextInt(12)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult3 == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult3 == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
                monresult4=ran.nextInt(12)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
                if (monresult4 == 9){
                    tvResult4.setImageResource(R.drawable.de9);
                }
                if (monresult4 == 10){
                    tvResult4.setImageResource(R.drawable.de10);
                }
                if (monresult4 == 11){
                    tvResult4.setImageResource(R.drawable.de11);
                }
                if (monresult4 == 12){
                    tvResult4.setImageResource(R.drawable.de12);
                }
            }

            if (choixface.equals("20 faces")){
                monresult=ran.nextInt(20)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                if (monresult == 13){
                    tvResult.setImageResource(R.drawable.de13);
                }
                if (monresult == 14){
                    tvResult.setImageResource(R.drawable.de14);
                }
                if (monresult == 15){
                    tvResult.setImageResource(R.drawable.de15);
                }
                if (monresult == 16){
                    tvResult.setImageResource(R.drawable.de16);
                }
                if (monresult == 17){
                    tvResult.setImageResource(R.drawable.de17);
                }
                if (monresult == 18){
                    tvResult.setImageResource(R.drawable.de18);
                }
                if (monresult == 19){
                    tvResult.setImageResource(R.drawable.de19);
                }
                if (monresult == 20){
                    tvResult.setImageResource(R.drawable.de20);
                }
                monresult2=ran.nextInt(20)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
                if (monresult2 == 13){
                    tvResult3.setImageResource(R.drawable.de13);
                }
                if (monresult2 == 14){
                    tvResult3.setImageResource(R.drawable.de14);
                }
                if (monresult2 == 15){
                    tvResult3.setImageResource(R.drawable.de15);
                }
                if (monresult2 == 16){
                    tvResult3.setImageResource(R.drawable.de16);
                }
                if (monresult2 == 17){
                    tvResult3.setImageResource(R.drawable.de17);
                }
                if (monresult2 == 18){
                    tvResult3.setImageResource(R.drawable.de18);
                }
                if (monresult2 == 19){
                    tvResult3.setImageResource(R.drawable.de19);
                }
                if (monresult2 == 20){
                    tvResult3.setImageResource(R.drawable.de20);
                }
                monresult3=ran.nextInt(20)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult3 == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult3 == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
                if (monresult3 == 13){
                    tvResult2.setImageResource(R.drawable.de13);
                }
                if (monresult3 == 14){
                    tvResult2.setImageResource(R.drawable.de14);
                }
                if (monresult3 == 15){
                    tvResult2.setImageResource(R.drawable.de15);
                }
                if (monresult3 == 16){
                    tvResult2.setImageResource(R.drawable.de16);
                }
                if (monresult3 == 17){
                    tvResult2.setImageResource(R.drawable.de17);
                }
                if (monresult3 == 18){
                    tvResult2.setImageResource(R.drawable.de18);
                }
                if (monresult3 == 19){
                    tvResult2.setImageResource(R.drawable.de19);
                }
                if (monresult3 == 20){
                    tvResult2.setImageResource(R.drawable.de20);
                }
                monresult4=ran.nextInt(20)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
                if (monresult4 == 9){
                    tvResult4.setImageResource(R.drawable.de9);
                }
                if (monresult4 == 10){
                    tvResult4.setImageResource(R.drawable.de10);
                }
                if (monresult4 == 11){
                    tvResult4.setImageResource(R.drawable.de11);
                }
                if (monresult4 == 12){
                    tvResult4.setImageResource(R.drawable.de12);
                }
                if (monresult4 == 13){
                    tvResult4.setImageResource(R.drawable.de13);
                }
                if (monresult4 == 14){
                    tvResult4.setImageResource(R.drawable.de14);
                }
                if (monresult4 == 15){
                    tvResult4.setImageResource(R.drawable.de15);
                }
                if (monresult4 == 16){
                    tvResult4.setImageResource(R.drawable.de16);
                }
                if (monresult4 == 17){
                    tvResult4.setImageResource(R.drawable.de17);
                }
                if (monresult4 == 18){
                    tvResult4.setImageResource(R.drawable.de18);
                }
                if (monresult4 == 19){
                    tvResult4.setImageResource(R.drawable.de19);
                }
                if (monresult4 == 20){
                    tvResult4.setImageResource(R.drawable.de20);
                }
            }
        }

        if (choixniveaux.equals("5 dés")){
            tvResult.setVisibility(View.VISIBLE);
            tvResult2.setVisibility(View.VISIBLE);
            tvResult3.setVisibility(View.VISIBLE);
            tvResult4.setVisibility(View.VISIBLE);
            tvResult5.setVisibility(View.VISIBLE);
            if (choixface.equals("4 faces")){
                monresult=ran.nextInt(4)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                monresult2=ran.nextInt(4)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                monresult3=ran.nextInt(4)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                monresult4=ran.nextInt(4)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                monresult5=ran.nextInt(4)+1;
                if (monresult5 == 1){
                    tvResult5.setImageResource(R.drawable.de1);
                }
                if (monresult5 == 2){
                    tvResult5.setImageResource(R.drawable.de2);
                }
                if (monresult5 == 3){
                    tvResult5.setImageResource(R.drawable.de3);
                }
                if (monresult5 == 4){
                    tvResult5.setImageResource(R.drawable.de4);
                }
            }

            if (choixface.equals("6 faces")){
                monresult=ran.nextInt(6)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                monresult2=ran.nextInt(6)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                monresult3=ran.nextInt(6)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                monresult4=ran.nextInt(6)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                monresult5=ran.nextInt(6)+1;
                if (monresult5 == 1){
                    tvResult5.setImageResource(R.drawable.de1);
                }
                if (monresult5 == 2){
                    tvResult5.setImageResource(R.drawable.de2);
                }
                if (monresult5 == 3){
                    tvResult5.setImageResource(R.drawable.de3);
                }
                if (monresult5 == 4){
                    tvResult5.setImageResource(R.drawable.de4);
                }
                if (monresult5 == 5){
                    tvResult5.setImageResource(R.drawable.de5);
                }
                if (monresult5 == 6){
                    tvResult5.setImageResource(R.drawable.de6);
                }
            }

            if (choixface.equals("8 faces")){
                monresult=ran.nextInt(8)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                monresult2=ran.nextInt(8)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                monresult3=ran.nextInt(8)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                monresult4=ran.nextInt(8)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
                monresult5=ran.nextInt(8)+1;
                if (monresult5 == 1){
                    tvResult5.setImageResource(R.drawable.de1);
                }
                if (monresult5 == 2){
                    tvResult5.setImageResource(R.drawable.de2);
                }
                if (monresult5 == 3){
                    tvResult5.setImageResource(R.drawable.de3);
                }
                if (monresult5 == 4){
                    tvResult5.setImageResource(R.drawable.de4);
                }
                if (monresult5 == 5){
                    tvResult5.setImageResource(R.drawable.de5);
                }
                if (monresult5 == 6){
                    tvResult5.setImageResource(R.drawable.de6);
                }
                if (monresult5 == 7){
                    tvResult5.setImageResource(R.drawable.de7);
                }
                if (monresult5 == 8){
                    tvResult5.setImageResource(R.drawable.de8);
                }
            }

            if (choixface.equals("10 faces")){
                monresult=ran.nextInt(10)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                monresult2=ran.nextInt(10)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                monresult3=ran.nextInt(10)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                monresult4=ran.nextInt(10)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
                if (monresult4 == 9){
                    tvResult4.setImageResource(R.drawable.de9);
                }
                if (monresult4 == 10){
                    tvResult4.setImageResource(R.drawable.de10);
                }
                monresult5=ran.nextInt(10)+1;
                if (monresult5 == 1){
                    tvResult5.setImageResource(R.drawable.de1);
                }
                if (monresult5 == 2){
                    tvResult5.setImageResource(R.drawable.de2);
                }
                if (monresult5 == 3){
                    tvResult5.setImageResource(R.drawable.de3);
                }
                if (monresult5 == 4){
                    tvResult5.setImageResource(R.drawable.de4);
                }
                if (monresult5 == 5){
                    tvResult5.setImageResource(R.drawable.de5);
                }
                if (monresult5 == 6){
                    tvResult5.setImageResource(R.drawable.de6);
                }
                if (monresult5 == 7){
                    tvResult5.setImageResource(R.drawable.de7);
                }
                if (monresult5 == 8){
                    tvResult5.setImageResource(R.drawable.de8);
                }
                if (monresult5 == 9){
                    tvResult5.setImageResource(R.drawable.de9);
                }
                if (monresult5 == 10){
                    tvResult5.setImageResource(R.drawable.de10);
                }
            }

            if (choixface.equals("12 faces")){
                monresult=ran.nextInt(12)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                monresult2=ran.nextInt(12)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
                monresult3=ran.nextInt(12)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult3 == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult3 == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
                monresult4=ran.nextInt(12)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
                if (monresult4 == 9){
                    tvResult4.setImageResource(R.drawable.de9);
                }
                if (monresult4 == 10){
                    tvResult4.setImageResource(R.drawable.de10);
                }
                if (monresult4 == 11){
                    tvResult4.setImageResource(R.drawable.de11);
                }
                if (monresult4 == 12){
                    tvResult4.setImageResource(R.drawable.de12);
                }
                monresult5=ran.nextInt(12)+1;
                if (monresult5 == 1){
                    tvResult5.setImageResource(R.drawable.de1);
                }
                if (monresult5 == 2){
                    tvResult5.setImageResource(R.drawable.de2);
                }
                if (monresult5 == 3){
                    tvResult5.setImageResource(R.drawable.de3);
                }
                if (monresult5 == 4){
                    tvResult5.setImageResource(R.drawable.de4);
                }
                if (monresult5 == 5){
                    tvResult5.setImageResource(R.drawable.de5);
                }
                if (monresult5 == 6){
                    tvResult5.setImageResource(R.drawable.de6);
                }
                if (monresult5 == 7){
                    tvResult5.setImageResource(R.drawable.de7);
                }
                if (monresult5 == 8){
                    tvResult5.setImageResource(R.drawable.de8);
                }
                if (monresult5 == 9){
                    tvResult5.setImageResource(R.drawable.de9);
                }
                if (monresult5 == 10){
                    tvResult5.setImageResource(R.drawable.de10);
                }
                if (monresult5 == 11){
                    tvResult5.setImageResource(R.drawable.de11);
                }
                if (monresult5 == 12){
                    tvResult5.setImageResource(R.drawable.de12);
                }
            }

            if (choixface.equals("20 faces")){
                monresult=ran.nextInt(20)+1;
                if (monresult == 1){
                    tvResult.setImageResource(R.drawable.de1);
                }
                if (monresult == 2){
                    tvResult.setImageResource(R.drawable.de2);
                }
                if (monresult == 3){
                    tvResult.setImageResource(R.drawable.de3);
                }
                if (monresult == 4){
                    tvResult.setImageResource(R.drawable.de4);
                }
                if (monresult == 5){
                    tvResult.setImageResource(R.drawable.de5);
                }
                if (monresult == 6){
                    tvResult.setImageResource(R.drawable.de6);
                }
                if (monresult == 7){
                    tvResult.setImageResource(R.drawable.de7);
                }
                if (monresult == 8){
                    tvResult.setImageResource(R.drawable.de8);
                }
                if (monresult == 9){
                    tvResult.setImageResource(R.drawable.de9);
                }
                if (monresult == 10){
                    tvResult.setImageResource(R.drawable.de10);
                }
                if (monresult == 11){
                    tvResult.setImageResource(R.drawable.de11);
                }
                if (monresult == 12){
                    tvResult.setImageResource(R.drawable.de12);
                }
                if (monresult == 13){
                    tvResult.setImageResource(R.drawable.de13);
                }
                if (monresult == 14){
                    tvResult.setImageResource(R.drawable.de14);
                }
                if (monresult == 15){
                    tvResult.setImageResource(R.drawable.de15);
                }
                if (monresult == 16){
                    tvResult.setImageResource(R.drawable.de16);
                }
                if (monresult == 17){
                    tvResult.setImageResource(R.drawable.de17);
                }
                if (monresult == 18){
                    tvResult.setImageResource(R.drawable.de18);
                }
                if (monresult == 19){
                    tvResult.setImageResource(R.drawable.de19);
                }
                if (monresult == 20){
                    tvResult.setImageResource(R.drawable.de20);
                }
                monresult2=ran.nextInt(20)+1;
                if (monresult2 == 1){
                    tvResult3.setImageResource(R.drawable.de1);
                }
                if (monresult2 == 2){
                    tvResult3.setImageResource(R.drawable.de2);
                }
                if (monresult2 == 3){
                    tvResult3.setImageResource(R.drawable.de3);
                }
                if (monresult2 == 4){
                    tvResult3.setImageResource(R.drawable.de4);
                }
                if (monresult2 == 5){
                    tvResult3.setImageResource(R.drawable.de5);
                }
                if (monresult2 == 6){
                    tvResult3.setImageResource(R.drawable.de6);
                }
                if (monresult2 == 7){
                    tvResult3.setImageResource(R.drawable.de7);
                }
                if (monresult2 == 8){
                    tvResult3.setImageResource(R.drawable.de8);
                }
                if (monresult2 == 9){
                    tvResult3.setImageResource(R.drawable.de9);
                }
                if (monresult2 == 10){
                    tvResult3.setImageResource(R.drawable.de10);
                }
                if (monresult2 == 11){
                    tvResult3.setImageResource(R.drawable.de11);
                }
                if (monresult2 == 12){
                    tvResult3.setImageResource(R.drawable.de12);
                }
                if (monresult2 == 13){
                    tvResult3.setImageResource(R.drawable.de13);
                }
                if (monresult2 == 14){
                    tvResult3.setImageResource(R.drawable.de14);
                }
                if (monresult2 == 15){
                    tvResult3.setImageResource(R.drawable.de15);
                }
                if (monresult2 == 16){
                    tvResult3.setImageResource(R.drawable.de16);
                }
                if (monresult2 == 17){
                    tvResult3.setImageResource(R.drawable.de17);
                }
                if (monresult2 == 18){
                    tvResult3.setImageResource(R.drawable.de18);
                }
                if (monresult2 == 19){
                    tvResult3.setImageResource(R.drawable.de19);
                }
                if (monresult2 == 20){
                    tvResult3.setImageResource(R.drawable.de20);
                }
                monresult3=ran.nextInt(20)+1;
                if (monresult3 == 1){
                    tvResult2.setImageResource(R.drawable.de1);
                }
                if (monresult3 == 2){
                    tvResult2.setImageResource(R.drawable.de2);
                }
                if (monresult3 == 3){
                    tvResult2.setImageResource(R.drawable.de3);
                }
                if (monresult3 == 4){
                    tvResult2.setImageResource(R.drawable.de4);
                }
                if (monresult3 == 5){
                    tvResult2.setImageResource(R.drawable.de5);
                }
                if (monresult3 == 6){
                    tvResult2.setImageResource(R.drawable.de6);
                }
                if (monresult3 == 7){
                    tvResult2.setImageResource(R.drawable.de7);
                }
                if (monresult3 == 8){
                    tvResult2.setImageResource(R.drawable.de8);
                }
                if (monresult3 == 9){
                    tvResult2.setImageResource(R.drawable.de9);
                }
                if (monresult3 == 10){
                    tvResult2.setImageResource(R.drawable.de10);
                }
                if (monresult3 == 11){
                    tvResult2.setImageResource(R.drawable.de11);
                }
                if (monresult3 == 12){
                    tvResult2.setImageResource(R.drawable.de12);
                }
                if (monresult3 == 13){
                    tvResult2.setImageResource(R.drawable.de13);
                }
                if (monresult3 == 14){
                    tvResult2.setImageResource(R.drawable.de14);
                }
                if (monresult3 == 15){
                    tvResult2.setImageResource(R.drawable.de15);
                }
                if (monresult3 == 16){
                    tvResult2.setImageResource(R.drawable.de16);
                }
                if (monresult3 == 17){
                    tvResult2.setImageResource(R.drawable.de17);
                }
                if (monresult3 == 18){
                    tvResult2.setImageResource(R.drawable.de18);
                }
                if (monresult3 == 19){
                    tvResult2.setImageResource(R.drawable.de19);
                }
                if (monresult3 == 20){
                    tvResult2.setImageResource(R.drawable.de20);
                }
                monresult4=ran.nextInt(20)+1;
                if (monresult4 == 1){
                    tvResult4.setImageResource(R.drawable.de1);
                }
                if (monresult4 == 2){
                    tvResult4.setImageResource(R.drawable.de2);
                }
                if (monresult4 == 3){
                    tvResult4.setImageResource(R.drawable.de3);
                }
                if (monresult4 == 4){
                    tvResult4.setImageResource(R.drawable.de4);
                }
                if (monresult4 == 5){
                    tvResult4.setImageResource(R.drawable.de5);
                }
                if (monresult4 == 6){
                    tvResult4.setImageResource(R.drawable.de6);
                }
                if (monresult4 == 7){
                    tvResult4.setImageResource(R.drawable.de7);
                }
                if (monresult4 == 8){
                    tvResult4.setImageResource(R.drawable.de8);
                }
                if (monresult4 == 9){
                    tvResult4.setImageResource(R.drawable.de9);
                }
                if (monresult4 == 10){
                    tvResult4.setImageResource(R.drawable.de10);
                }
                if (monresult4 == 11){
                    tvResult4.setImageResource(R.drawable.de11);
                }
                if (monresult4 == 12){
                    tvResult4.setImageResource(R.drawable.de12);
                }
                if (monresult4 == 13){
                    tvResult4.setImageResource(R.drawable.de13);
                }
                if (monresult4 == 14){
                    tvResult4.setImageResource(R.drawable.de14);
                }
                if (monresult4 == 15){
                    tvResult4.setImageResource(R.drawable.de15);
                }
                if (monresult4 == 16){
                    tvResult4.setImageResource(R.drawable.de16);
                }
                if (monresult4 == 17){
                    tvResult4.setImageResource(R.drawable.de17);
                }
                if (monresult4 == 18){
                    tvResult4.setImageResource(R.drawable.de18);
                }
                if (monresult4 == 19){
                    tvResult4.setImageResource(R.drawable.de19);
                }
                if (monresult4 == 20){
                    tvResult4.setImageResource(R.drawable.de20);
                }
                monresult5=ran.nextInt(20)+1;
                if (monresult5 == 1){
                    tvResult5.setImageResource(R.drawable.de1);
                }
                if (monresult5 == 2){
                    tvResult5.setImageResource(R.drawable.de2);
                }
                if (monresult5 == 3){
                    tvResult5.setImageResource(R.drawable.de3);
                }
                if (monresult5 == 4){
                    tvResult5.setImageResource(R.drawable.de4);
                }
                if (monresult5 == 5){
                    tvResult5.setImageResource(R.drawable.de5);
                }
                if (monresult5 == 6){
                    tvResult5.setImageResource(R.drawable.de6);
                }
                if (monresult5 == 7){
                    tvResult5.setImageResource(R.drawable.de7);
                }
                if (monresult5 == 8){
                    tvResult5.setImageResource(R.drawable.de8);
                }
                if (monresult5 == 9){
                    tvResult5.setImageResource(R.drawable.de9);
                }
                if (monresult5 == 10){
                    tvResult5.setImageResource(R.drawable.de10);
                }
                if (monresult5 == 11){
                    tvResult5.setImageResource(R.drawable.de11);
                }
                if (monresult5 == 12){
                    tvResult5.setImageResource(R.drawable.de12);
                }
                if (monresult5 == 13){
                    tvResult5.setImageResource(R.drawable.de13);
                }
                if (monresult5 == 14){
                    tvResult5.setImageResource(R.drawable.de14);
                }
                if (monresult5 == 15){
                    tvResult5.setImageResource(R.drawable.de15);
                }
                if (monresult5 == 16){
                    tvResult5.setImageResource(R.drawable.de16);
                }
                if (monresult5 == 17){
                    tvResult5.setImageResource(R.drawable.de17);
                }
                if (monresult5 == 18){
                    tvResult5.setImageResource(R.drawable.de18);
                }
                if (monresult5 == 19){
                    tvResult5.setImageResource(R.drawable.de19);
                }
                if (monresult5 == 20){
                    tvResult5.setImageResource(R.drawable.de20);
                }
            }
        }

        tvMonRecap.setText(monrecap);

        String monrep;
        if (choixniveaux == null){
            monrep = "Vous avez choisit 1 dés ";
        }else {
            monrep = "Vous avez choisit " + mIntent.getStringExtra("Spinner2");
        }
        tvMonRecap2.setText(monrep);

        buttonReLancer.setOnClickListener(new View.OnClickListener() {
        @Override

        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(),MainResult.class);
            startActivity(intent);
            toRecap2();
        }
    });
}
    public void toRecap(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
    public void toRecap2(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}